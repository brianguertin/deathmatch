var game = new Phaser.Game(window.innerWidth, window.innerHeight, Phaser.AUTO, '', { preload: preload, create: create, update: update });
var myFly;

var scope = {};

var peerConnections = [];

function preload() {
	game.load.image('grass', 'grass.jpg');
	game.load.image('bullet', 'bullet.png');
	game.load.image('tank', 'tank.png');
	game.load.audio('buzz', 'buzz.wav');
}

function create() {
    game.stage.disableVisibilityChange = true;
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.add.tileSprite(0, 0, game.width, game.height, 'grass');
    scope.flies = game.add.group();
    scope.bullets = game.add.group();
	scope.buzzing = game.add.audio('buzz');
    
    var key1 = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    key1.onDown.add(fire, this);
    
    myFly = createFly();
    
    var initPeerConnection = function(conn) {
        var fly = createFly();
        conn.on('data', function(data){
            fly.position.setTo(data.position.x, data.position.y);
            fly.body.velocity.setTo(data.velocity.x, data.velocity.y);
            if (data.action && data.action == 'fire') {
                createBullet(fly.position, data.angle);
            }
        });
        conn.on('close', function(peer){
            fly.kill();
            var index = peerConnections.indexOf(conn);
            if (index > -1) {
                peerConnections.splice(index, 1);
            }
        });
        peerConnections.push(conn);
    };

    var peer = new Peer({key: 'if1ed37ma7bsxlxr'});
    peer.on('open', function(id) {
        if (!location.hash) {
            $('#share').attr('href', location.href + '#' + id);
        }
    });
    peer.on('connection', function(conn) {
        initPeerConnection(conn);
    });

    if (location.hash) {
        $('#share').attr('href', location.href);
        var conn = peer.connect(location.hash.substr(1));
        conn.on('open', function() {
            initPeerConnection(conn);
        });
    }
}

function update () {
    var speed = 100;
    var velocity = new Phaser.Point(0, 0);
    if (game.input.keyboard.isDown(Phaser.Keyboard.UP)) {
        velocity.y -= 1;
    }
    if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN)) {
        velocity.y += 1;
    }
    if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
        velocity.x -= 1;
    }
    if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
        velocity.x += 1;
    }
    velocity.normalize();
    myFly.body.velocity.setTo(velocity.x * speed, velocity.y * speed);
	scope.flies.forEach(function(fly) {
		setRotationFromVelocity(fly);
	});
    broadcast({});
    game.physics.arcade.collide(scope.flies, scope.flies, null, null, this);
    game.physics.arcade.overlap(scope.flies, scope.bullets, function(tank, bullet) {
        bullet.kill();
    }, null, this);
}

function broadcast(packet) {
    packet.position = {x:myFly.position.x, y:myFly.position.y};
    packet.velocity = {x:myFly.body.velocity.x, y:myFly.body.velocity.y};
    for (var i = 0; i < peerConnections.length; ++i) {
        peerConnections[i].send(packet);
    }
}

function createBullet(position, angle) {
    var fly = scope.bullets.create(position.x, position.y, 'bullet');
    fly.anchor.setTo(0.5, 0.5);
    
    var scale = 0.25;
    fly.scale.setTo(scale, scale);
    
    var tankScale = 0.5;
    var p = new Phaser.Point(0, -125 * tankScale);
    p.rotate(0, 0, angle);
    fly.position.add(p.x, p.y);
    
    game.physics.enable(fly, Phaser.Physics.ARCADE);
    //fly.body.collideWorldBounds = true;
    fly.body.velocity.setTo(0, -400);
    fly.body.velocity.rotate(0, 0, angle);
    setRotationFromVelocity(fly);
    
    fly.checkWorldBounds = true;
    fly.events.onOutOfBounds.add(function(bullet) {
        bullet.kill();
    }, this);
}

function fire() {
    var angle = Phaser.Math.degToRad(myFly.angle);
    broadcast({action:'fire', angle:angle});
    createBullet(myFly.position, angle);
}

function setRotationFromVelocity(sprite) {
    if (sprite.body.velocity.x == 0 && sprite.body.velocity.y == 0) {
        return;
    }
	sprite.rotation = game.math.angleBetween(0, 0, sprite.body.velocity.x, sprite.body.velocity.y) + Math.PI / 2;
}

function createFly() {
    var fly = scope.flies.create(game.width / 2, game.height / 2, 'tank');
    fly.anchor.setTo(0.5, 0.5);
    
    var scale = 0.5;
    fly.scale.setTo(scale, scale);
    
    game.physics.enable(fly, Phaser.Physics.ARCADE);
    fly.body.collideWorldBounds = true;
    
	scope.buzzing.play();
    
    return fly;
}
